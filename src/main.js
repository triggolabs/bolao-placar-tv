import App from './App.html';
import { Store } from 'svelte/store';
import { parseQuery } from './UrlHelper';

const scoreSize = parseQuery(window.location.search).scoreSize || 1;

const store = new Store({
	scoreSize,
});

var app = new App({
	target: document.body,
	store
});

fetch('http://bolao.sysmap.com.br/api/v1/addresses/N8yv1hhUSBhZFJ3oJyJwBcBSjhyWT1SPWipZtUSVaswRiAUpNFuBVxiiVvM89Vc5LAVy4PQwKwKoXDhkTjj3q5aoQM9VTpr/bets')
.then(res => res.json())
.then(res => { console.log(res) });

export default app;