# Bolão Placar TV

Install the dependencies...

```bash
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src` or a static file in `public`, save it, and reload the page to see your changes.


## Deploying to the web

```bash
npm run build
```

The folder `public` will cointain all production files.
